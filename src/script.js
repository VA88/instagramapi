(function(window) {
  const ACCESS_TOKEN = "251170693.d15ed18.edbe599aa3cc4e2599ee13e360ec5ea5";
  const USER_DATA_URL = `https://api.instagram.com/v1/users/self/?access_token=${ACCESS_TOKEN}`;
  const USER_MEDIA_URL = `https://api.instagram.com/v1/users/self/media/recent?access_token=${ACCESS_TOKEN}&count=5`;
  let nextMediaURL;

  const init = async function() {
      
    const userDataResponse = await fetch(USER_DATA_URL);
    if (userDataResponse.ok) {
      const userDataJSON = await userDataResponse.json();
      //console.log(userDataJSON);
      insertProfileData(userDataJSON.data);  
    } else {
      throw Error(userDataResponse.status);
    }
      
    await fetchPosts(USER_MEDIA_URL);
  }
  
  async function fetchPosts(URL) {
    const userPostsResponse = await fetch(URL);
    if (userPostsResponse.ok) {
      const userPostsJSON = await userPostsResponse.json();
      //console.log(userPostsJSON);
      if (!nextMediaURL) {document.getElementById("posts").innerHTML = "";}
      insertPostData(userPostsJSON.data);
          
      if (!nextMediaURL) {
        const elementButton = document.createElement("button");
        elementButton.className = "huge ui button";   
        elementButton.id = "loadMoreButton";   
        elementButton.onclick = function() {loadMore();};  
        elementButton.innerHTML = "Load More";   
        document.getElementById("buttonContainer").append(elementButton);
      }
        
      nextMediaURL = userPostsJSON.pagination.next_url;
      if (!nextMediaURL) {document.getElementById("loadMoreButton").disabled = true;}
    } else {
      throw Error(userPostsResponse.status);
    }
  }
    
  window.loadMore = async function() {
    await fetchPosts(nextMediaURL); 
  }
    
    
  function insertProfileData(userData) {
    if (!userData) return;
    document.getElementById('username').innerHTML = userData.full_name;
    document.getElementById('postsCount').innerHTML = userData.counts.media + " " + (userData.counts.media == 1 ? "post" : "posts"); 
    document.getElementById('userPhoto').src = userData.profile_picture;
  }
    
  function insertPostData(posts) {
    if (!posts) return;
      
    posts.forEach(function(post) {
        
      const divCard = document.createElement("div");   
      divCard.className = "ui card";
    
      const divImg = document.createElement("div");   
      divImg.className = "image";
      divImg.innerHTML = `<img src="${post.images.standard_resolution.url}">`;
      divCard.append(divImg);
      
      const divContent = document.createElement("div");
      divContent.className = "content";
      if (post.caption) {
        divContent.innerHTML = `<a class="header">${cutTags(post.caption.text)}</a>`;
      } 
        
      const divMeta = document.createElement("div");
      divMeta.className = "meta";
      divMeta.innerHTML = `<span class="date">${timeConverter(post.created_time)}</span>`;
      divContent.append(divMeta);
        
      if (post.tags.length > 0) {    
        const divDesc = document.createElement("div");
        divDesc.className = "description";
        createTags(post.tags, divDesc);
        divContent.append(divDesc);
      }
    
      divCard.append(divContent);
        
      const divExtra = document.createElement("div");
      divExtra.className = "extra content";
      divExtra.innerHTML = `<span class="right floated"><i class="comment icon"></i>${post.comments.count} Comments</span>` +
        `<i class="heart like outline icon"></i>${post.likes.count} Likes`;
      divCard.append(divExtra);
       
      document.getElementById('posts').append(divCard);
        
    });
  }
    
    
  function timeConverter(UNIX_timestamp) {
    const a = new Date(UNIX_timestamp * 1000);
    const year = a.getFullYear();
    let month = a.getMonth();
    month++;
    if (month < 10) {month = "0" + month;} 
    const date = a.getDate();
    const time = date + "." + month + "." + year;
    return time;
  }
    
  function cutTags(string) {
    const charIndex = string.indexOf("#");
    if (charIndex == -1) {return string;}
    const result = string.substr(0, charIndex);
    return result;
  }
    
  function createTags(tags, parent) {
    if (!tags) return;
    tags.forEach(function(tag) {
       const aContent = document.createElement('a');
       aContent.innerHTML = "#" + tag + " ";
       parent.append(aContent);
    });     
  }
    
  init();
})(window);
